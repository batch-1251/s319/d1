const express=require('express');
const mongoose=require('mongoose');//is code to be used on our db connection and the reate of our schema and model for our existing mongodb atlas collection.

const app=express();// creating a server through the use of app variable
const port=3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));


//-a way to connect our mongodb atlas db connection string to our server
mongoose.connect("mongodb+srv://louie011:lowiidr011@cluster0.7ajhf.mongodb.net/b125-tasks?retryWrites=true&w=majority", 
				{useNewUrlParser:true, useUnifiedTopology:true})
			.then(()=>{console.log("Successfully Connected to Database")})//if t he mongoose suceeded on the connection, then we will console.log message
			.catch(error=>{console.log(error)});//handles error when the mongoose failed to connect on our mongodb atlas database


/*-----------------------------------------------------------------------*/
/*Schema-gives a structure of what kind of record/document we are going to contain our database*/
//Schema() method-determins the structure of the documents to be written in the database
	//Schema acts as blueprint to our data
	//use the Schema() constructor of the MOngoose dependency to create a new Schema object for our tasks collection

const taskSchema=new mongoose.Schema(
	{
		name:String,
		status:{type:Boolean, default:false}
	}
);

/*Models to perform the CRUD operation for our defined collections with coreeesponding Schema*/
const Task=mongoose.model('Task', taskSchema);



//mini activity
const userSchema=new mongoose.Schema(
	{
		firstName:String,
		lastName:String,
		userName:String,
		passWord:String
		
	}
);
const User=mongoose.model('User', userSchema);

/*
	Business Logic-Todo list application 
		-CRUD operation for our Task collection
*/
//insert new task
app.post('/add-task', (req, res)=>{//call the model for our task collection
	
	let newTask= new Task({
		name:req.body.name

	});
	newTask.save((error,savedTask)=>{
		if(error){console.log(error)}
		else{res.send(`New task saved! ${savedTask}`)}
	});
})

// app.post("/add_user", async(req, res) => {
// 	const user = new userModel(req.body);
// // async & await
// // try & catch
// 	try {
// 		await user.save();
// 		res.send(user);
// 	} catch (error) {
// 		res.status(500).send(error);
// 	}

// });

app.post('/register', (req, res)=>{
	let newUser= new User({
		firstName:req.body.firstName,
		lastName:req.body.lastName,
		userName:req.body.userName,
		passWord:req.body.passWord

	});
	newUser.save((error,savedUser)=>{
		if(error){console.log(error)}
		else{res.send(`New User saved! ${savedUser}`)}
	});
})


//Retrieve

app.get('/retrieve-tasks',(req,res)=>{
	Task.find({},(error,records)=>{
		if(error){console.log(error)}
		else{res.send(records)}
	})
});

app.get('/retrieve-tasks-done',(req,res)=>{
	Task.find({status:true},(error, records)=>{
		if(error){console.log(error)}
		else{res.send(records)}
	})
});

//update operation
app.put('/complete-task/:taskId', (req, res)=>{

	// res.send({urlParams:req.params.taskId})
	/* 1.find the specific record
		2.update 
	*/
	let taskId=req.params.taskId
		//url parameters-there are the values defined on the URLs
		//to get the url parameters-req.params.<paramsNames>
		//:taskId- is a way to indicate that we are going to recieve a url parameter,thes are what wer call a 'wildcard'

	Task.findByIdAndUpdate(taskId,{status:true},(error, updateTask)=>{
		if(error){console.log(error)}
		else{res.send(`Task completed successfully!`)}
	})

});

//Delete
app.delete('/delete-task/:taskId',(req, res)=>{
	//findByIdAndDelete()
	let taskId=req.params.taskId
	Task.findByIdAndDelete(taskId,(error, deleteTask)=>{
		if(error){console.log(error)}
			else{res.send('Task deleted!')}
	})
})







app.listen(port, ()=>console.log(`server is running at port ${port}`));